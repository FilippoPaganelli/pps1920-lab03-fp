package u02

import org.junit.jupiter.api.Assertions._
import org.junit.jupiter.api.Test
import u02.Modules._
import u03.Lists.List._
import u03.Streams._

class Lab02Test {
  import u03.Lists._

  @Test def testDrop(): Unit ={
    val lst = List.Cons(10, List.Cons(20, List.Cons(30, List.Nil())))
    assertEquals(List.drop(lst, 1), List.Cons(20, List.Cons(30, List.Nil())))
    assertEquals(List.drop(lst, 2), List.Cons(30, List.Nil()))
    assertEquals(List.drop(lst, 5), List.Nil())
  }

  @Test def testFlatMap(): Unit = {
    val lst = List.Cons(10, List.Cons(20, List.Cons(30, List.Nil())))
    assertEquals(flatMap(lst)(v => Cons(v+1, Nil())), List.Cons(11, List.Cons(21, List.Cons(31, List.Nil()))))
  }

  @Test def testMap(): Unit = {
    val lst = List.Cons(10, List.Cons(20, List.Cons(30, List.Nil())))
    assertEquals(List.mapWithFlatMap(lst)(v => v+1), List.Cons(11, List.Cons(21, List.Cons(31, List.Nil()))))
  }

  import u02.Optionals._
  @Test def testMax(): Unit = {
    val lst = List.Cons(50, List.Cons(20, List.Cons(30, List.Nil())))
    assertEquals(Option.Some(50), List.max(lst))
    assertEquals(Option.None(), List.max(List.Nil()))
  }

  @Test def testFoldLeft(): Unit ={
    val lst2 = Cons(3, Cons(7, Cons(1, Cons(5, Nil()))))
    assertEquals(-16, List.foldLeft(lst2)(0)(_-_))
  }

  @Test def testFoldRight(): Unit ={
    val lst2 = Cons(3, Cons(7, Cons(1, Cons(5, Nil()))))
    assertEquals(-8, List.foldRight(lst2)(0)(_-_))
  }

  @Test def testTeacherCourses(): Unit = {
    val p1: Person = Person.Student("mario",2015)
    val p2: Person = Person.Student("mario",2015)
    val p3: Person = Person.Student("mario",2015)
    val p4: Person = Person.Teacher("mario","OOP")
    val p5: Person = Person.Teacher("mario","SD")
    val p6: Person = Person.Teacher("mario","ASW")

    val personList = Cons(p1, Cons(p2, Cons(p3, Cons(p4, Cons(p5, Cons(p6, Nil()))))))

    assertEquals(Cons("OOP",Cons("SD",Cons("ASW",Nil()))), teachersCoursesList(personList))
  }

  @Test def testDropStream(): Unit = {
    val s = Stream.take(Stream.iterate(0)(_+1))(10)
    val expected = Cons(6, Cons(7, Cons(8, Cons(9, Nil()))))
    assertEquals(expected, Stream.toList(Stream.drop(s)(6)))
  }

  @Test def testConstant(): Unit = {
    val expected = Cons("x", Cons("x", Cons("x", Cons("x", Cons("x", Nil())))))
    assertEquals(expected, Stream.toList(Stream.take(Stream.constant("x"))(5)))
  }

  @Test def TestFibonacci(): Unit = {
    val expected = Cons(0, Cons(1, Cons(1, Cons(2, Cons(3, Cons(5, Cons(8, Cons(13, Nil()))))))))
    assertEquals(expected, Stream.toList(Stream.take(Stream.fibs)(8)))
  }

}
